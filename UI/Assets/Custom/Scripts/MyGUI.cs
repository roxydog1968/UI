﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[ExecuteInEditMode]
public class MyGUI : MonoBehaviour {
	public class ButtonSet{
		public Rect left = new Rect(5, 30, 100, 20);
		public Rect Right = new Rect(105, 30, 100, 20);
		public Rect up = new Rect(50, 10, 100, 20);
		public Rect down = new Rect(50, 50, 100, 20);

	}
	public Transform t;
	public ButtonSet guiButtons = new ButtonSet();
	public player Player;
	void OnGUI() {

		if (GUI.RepeatButton (guiButtons.left, "left"))
		{
			player.instance.Move (Vector3.left);
		}
		if (GUI.RepeatButton (guiButtons.Right, "Right"))
		{
			
			player.instance.Move (Vector3.right);
		}

		if (GUI.RepeatButton (guiButtons.up, "Up"))
			
		{
			
			player.instance.Move (Vector3.up);
		}

		if (GUI.RepeatButton (guiButtons.down, "Down"))	{
			player.instance.Move (Vector3.down);
			}
	}
			
}
	


	

