﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {
	public float speed = 10.0f;

	public static player instance;

	void Awake(){
		instance = this;
	}

	public void Move(Vector3 _direction) { 			
		transform.position += _direction *speed* Time.deltaTime;
	}
}