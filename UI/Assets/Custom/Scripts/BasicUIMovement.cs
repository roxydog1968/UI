﻿using System;
using UnityEngine;
using UnityEngine.UI;
public class BasicUIMovement : MonoBehaviour {
	[Serializable]
	public class ButtonSet {
		public Button left;
		public Button right;
}
	public ButtonSet buttons;

	void Awake() {
		buttons.left.onClick.AddListener (() => {player.instance.Move(Vector3.left); });
		buttons.right.onClick.AddListener (() => {player.instance.Move(Vector3.right); });

	}

}
