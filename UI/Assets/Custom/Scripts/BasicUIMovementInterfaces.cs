﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BasicUIMovementInterfaces : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	public enum Direction{
		Left,
		Right
	}
	public Direction direction = Direction.Left;
	private float power;
	private bool isHeld;

	void Update() {
		if (Mathf.Abs(power) > 0) 
		player.instance.Move (Vector3.right * power);
	}
	public void OnPointerEnter(PointerEventData eventData){
		isHeld = true;
		power = direction == Direction.Left ? -1 : 1;
	}

	public void OnpointerExit(PointerEventData eventData) {
		power = 0;
	}

}
